package com.zanexample.trafficlight;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Button buttonRed,
                   buttonYellow,
                   buttonGreen;

    private  int  fon;
    private LinearLayout TrafficLight;

    private static final String FON_COLOR = "FON_COLOR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TrafficLight = (LinearLayout)findViewById(R.id.TrafficLight);

        buttonRed = (Button) findViewById(R.id.button_red);
        buttonYellow = (Button) findViewById(R.id.button_yellow);
        buttonGreen = (Button) findViewById(R.id.button_green);
        if(savedInstanceState != null){
            //fon = savedInstanceState.getInt(FON_COLOR);
            TrafficLight.setBackgroundColor( fon = savedInstanceState.getInt(FON_COLOR) );
        }

        buttonRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setColorFon(buttonRed);
            }
        });

        buttonYellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setColorFon(buttonYellow);
            }
        });

        buttonGreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setColorFon(buttonGreen);
            }
        });
    }

    public void setColorFon(Button button) {
        fon = button.getCurrentTextColor();
        TrafficLight.setBackgroundColor(fon);
    }

    @Override
    public void onSaveInstanceState(Bundle saveInstanceState){
        saveInstanceState.putInt(FON_COLOR, fon);
        super.onSaveInstanceState(saveInstanceState);
    }
}