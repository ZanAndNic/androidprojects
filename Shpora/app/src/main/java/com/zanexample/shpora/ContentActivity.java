package com.zanexample.shpora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

public class ContentActivity extends AppCompatActivity {
    private TextView contentView;
    private String[] nameFiles;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_activity);

        contentView = (TextView) findViewById(R.id.listView);
        nameFiles = getResources().getStringArray(R.array.array_file);
        position = getIntent().getIntExtra(MainActivity.KEY_ID, 0);

        InputFile file = new InputFile(getApplicationContext(), nameFiles[position]);

        contentView.setText(Html.fromHtml(file.readFile()));
    }
}
