package com.zanexample.gps_2;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.UUID;

/**
 * Created by Andrey Zavarin and Pavel Kulkov on 19.05.2016.
 * Класс для работы c устройством по bluetooth
 *
 */
public class BluetoothManager {

    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothSocket bluetoothSocket;
    private BluetoothDevice bluetoothDevice;
    private final String TAG = "GPS";
    private InputStream inputStream;
    private OutputStream outputStream;

    public BluetoothManager(BluetoothDevice bluetoothDevice){
        this.bluetoothDevice = bluetoothDevice;
        try{
            bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(MY_UUID);
            Log.d(TAG, "... Получение сокета ...");
        } catch (IOException e) {
            Log.d(TAG, "... Ощибка, сокет не получен - " + e);
        }
    }

    /**
    * Метод для подключения к устройству
     *
    * @return isConnected true если подключение прошло успешно, иначе false
    * */
    public boolean deviceConnect(){
        boolean isConnected = true;
        try {
            bluetoothSocket.connect();
            Log.d(TAG, "... Соединение установлено, готовность к передаче ...");
            MapActivity.switchConnectGps.setChecked(true);
        } catch (IOException e) {
            isConnected = false;
            try{
                bluetoothSocket.close();
                Log.d(TAG, "... Сокет закрыт ...");
            }
            catch (IOException e2){
                Log.d(TAG, "... Ошибка закрытия сокета! - " + e2);
            }
        }
        return isConnected;
    }

    /**
     * Метод для подключения к входному, выходному потокам
     *
     * @return isConnected true если подключение прошло успешно, иначе false
     * */
    public boolean streamsConnect(){
        boolean isConnected = true;
        try{
            inputStream = bluetoothSocket.getInputStream();
            //outputStream = bluetoothSocket.getOutputStream();
        }
        catch (IOException e){
            Log.d(TAG, "... Ошибка, сокет не создан - " + e);
            isConnected = false;
        }
        return isConnected;
    }

    /**
     * Метод для получения данных из входного потока
     *
     * */
    public void getDataFromInputStream(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String line="";
        int count=0;
        NmeaParser nmeaParser = new NmeaParser();
        //Отправляем данные из входного потока в парсер
        try {
            while (true) {
                if((line = bufferedReader.readLine()) != null && count < 10){
                    nmeaParser.run(line);
                    count++;
                }else{
                    //После 10 отправлений делаем sleep чтобы ui поток отрисовал
                    Thread.sleep(100);
                    count = 0;
                }
            }
        }
        catch (IOException e){
            try {
                bluetoothSocket.close();
                bufferedReader.close();
            }
            catch (IOException e2){
                Log.d(TAG, "... Ошибка закрытия сокета! - " + e2);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}