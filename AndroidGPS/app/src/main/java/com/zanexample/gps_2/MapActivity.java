package com.zanexample.gps_2;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.compass.CompassOverlay;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Andrey Zavarin and Pavel Kulkov on 19.05.2016.
 * Класс для работы c устройством по bluetooth
 *
 */

public class MapActivity extends AppCompatActivity {
    private static BluetoothDevice bluetoothDevice;
    private static BluetoothAdapter bluetoothAdapter;
    private static final String TAG = "GPS";
    private static final String address = "00:12:6F:32:FF:DD";
    private static Coordinate coordinate = Coordinate.getINSTANCE();

    public boolean animateCenter = false;
    public static Switch switchConnectGps;

    public ArrayList<GeoPoint> listMarker = new ArrayList<GeoPoint>();

    public MapView map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        map = (MapView) findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMaxZoomLevel(30);
        map.setMultiTouchControls(true);
        //map.setUseDataConnection(false);//keeps the mapView from loading online tiles using network connection.
        map.getController().setZoom(18);
        map.setCameraDistance(10);

        CompassOverlay compassOverlay = new CompassOverlay(this, map);
        compassOverlay.enableCompass();
        map.getOverlays().add(compassOverlay);

        ScaleBarOverlay scaleBarOverlay = new ScaleBarOverlay(map);
        scaleBarOverlay.setAlignBottom(true);
        scaleBarOverlay.setAlignRight(true);
        map.getOverlays().add(scaleBarOverlay);



        switchConnectGps = (Switch) findViewById(R.id.switchConnectGps);
        switchConnectGps.setChecked(false);

        /*switchConnectGps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.v("Switch State=", "" + isChecked);
            }
        });*/



        final Switch animate = (Switch) findViewById(R.id.animate);
        animate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    animateCenter = true;
                    Toast.makeText(getApplicationContext(), "Отслеживание движение включено",
                            Toast.LENGTH_SHORT).show();
                } else {
                    animateCenter = false;
                    Toast.makeText(getApplicationContext(), "Отслеживание движение отключенр",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        //Если bluetooth есть на девайсе
        if (bluetoothAdapter == null) {
            Log.d(TAG, "Bluetooth не поддерживается");
        } else {
            if (bluetoothAdapter.isEnabled()) {
                Log.d(TAG, "... Bluetooth включен");
            } else {
                Log.d(TAG, "... Попытка включения Bluetooth");
                Intent enableBTIntent = new Intent(bluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBTIntent, 0);
                while (true) {
                    Log.d(TAG, "... " + bluetoothAdapter.isEnabled());
                    if (bluetoothAdapter.isEnabled()) {
                        Log.d(TAG, "... Bluetooth включен ...");
                        break;
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        //Получаем gps
        bluetoothDevice = bluetoothAdapter.getRemoteDevice(address);

        final BluetoothManager bluetoothManager = new BluetoothManager(bluetoothDevice);

        if (bluetoothManager.deviceConnect()) {
            if (bluetoothManager.streamsConnect()) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        //Запускаем поток для получения данных с gps-устройства
                        bluetoothManager.getDataFromInputStream();
                    }
                };

                Thread thread = new Thread(runnable);
                thread.setDaemon(true);
                thread.setName("getDataFromInputStream");
                thread.start();
            }
        }
        onMapReady();
        //final int count = 0;

        /*map.setOnTouchListener(new MapView.OnTouchListener(){

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEvent.ACTION_DOWN != event.getAction()){
                    return false;
                }
                Log.d("КАСАНИЕ", "НАЖАЛ");
                count++;



                return true;


            }
        });*/

        MapEventsReceiver mapEventsReceiver = new MapEventsReceiver() {
            @Override
            public boolean singleTapConfirmedHelper(GeoPoint p) {
                Toast.makeText(getApplicationContext(), "Нажал", Toast.LENGTH_SHORT).show();
                setMarker(p);
                return false;
            }

            @Override
            public boolean longPressHelper(GeoPoint p) {
                Toast.makeText(getApplicationContext(), "зажал", Toast.LENGTH_SHORT).show();
                return false;
            }
        };
        MapEventsOverlay OverlayEventos = new MapEventsOverlay(getBaseContext(), mapEventsReceiver);
        map.getOverlays().add(OverlayEventos);
    }

    public void setMarker(IGeoPoint iGeoPoint){
        GeoPoint tmp = (GeoPoint) iGeoPoint;

        Marker marker = new Marker(map);
        marker.setPosition(tmp);
        marker.setTitle("1");
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        map.getOverlays().add(marker);

        listMarker.add(tmp);
        Toast.makeText(getApplicationContext(), listMarker.size() + " ", Toast.LENGTH_SHORT).show();

       if(listMarker.size() == 2){
            Polyline polyline = new Polyline(this);
            polyline.setColor(Color.RED);
            polyline.setVisible(true);
            polyline.setWidth(5.0f);
            //Добавляем пустую линию на карту
            map.getOverlays().add(polyline);
            polyline.setPoints(listMarker);


            //listMarker.clear();
           double dis = Coordinate.distVincenty(listMarker.get(0).getLatitude(), listMarker.get(0).getLongitude(), listMarker.get(1).getLatitude(), listMarker.get(1).getLongitude());

           Toast.makeText(getApplicationContext(), "Дистанция =  " + dis, Toast.LENGTH_SHORT).show();
        }


    }

    public void onMapReady(){
        final Polyline polyline = new Polyline(this);
        polyline.setColor(Color.BLUE);
        polyline.setVisible(true);
        polyline.setWidth(5.0f);
        //Добавляем пустую линию на карту
        map.getOverlays().add(polyline);

        //Запускаем поток для отрисовки трека
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(!gpsReady()){
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                final Marker marker = new Marker(map);
                final GeoPoint tmp = (GeoPoint)coordinate.getArrayLatsLngs()[0];
                marker.setPosition(tmp);
                marker.setTitle("Начало");
                marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //Добавляем маркер в первую точку
                        map.getOverlays().add(marker);
                        map.getController().animateTo(tmp);
                    }
                });
                for (;;) {
                    //Получаем лист с точками
                    final List latLngs = Arrays.asList(coordinate.getArrayLatsLngs());
                    final GeoPoint lastPoint = (GeoPoint)latLngs.get(latLngs.size()-1);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //Рисуем линию с новыми точками
                            polyline.setPoints(latLngs);
                            //Перемещаем камеру в первую точку
                            if(animateCenter){
                                map.getController().setCenter(lastPoint);
                            }

                            //map.setCameraDistance(10);
                        }
                    });
                    try {
                        //Отрисовываем 2 раза в 1 сек
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.setDaemon(true);
        thread.setName("drawThread");
        thread.start();
    }

    public static boolean gpsReady(){
        if(coordinate.getArrayLatsLngs().length != 0){
            return true;
        }else {
            return false;
        }
    }
}
