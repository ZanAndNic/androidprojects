package com.zanexample.gps_2;

import android.util.Log;
import org.osmdroid.util.GeoPoint;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by Andrey on 06.05.2016.
 * Данный клас позволяет получить данные из GPS потока
 *
 * @version 1.2
 * @author Заварин А.Н.
 */
public class NmeaParser{
    private static final String TAG = "NmeaParser";

    private static final TimeZone TIME_ZONE = TimeZone.getTimeZone("UTC");

    private String mName;

    private String[] masMName;

    private String[] masGGA;

    private int mYear = -1;
    private int mMonth;
    private int mDay;

    private Coordinate coordinate = Coordinate.getINSTANCE();

    //Широта
    private double mLatitude;
    //Долгота
    private double mLongitude;
    //Направление движения
    public static float bearing;
    //HDOP
    private double HDOP;
    //Скорость
    private double speed;

    private File file = null;

    private PrintWriter writer=null;


    public NmeaParser(){
        try {
            file = new File("/sdcard/","LogsGps.txt");
            writer = new PrintWriter(new BufferedWriter(new FileWriter(file,true)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Запуск основных методов для работы с GPS строкой
     *
     * @param name строка GPS данных
     */
    public void run(String name){
        if(name.length() > 6){
            switch (name.substring(0, 6)){
                case "$GNRMC":
                case "$GPRMC":
                    runRMC(name);
                    break;
                case "$GNGGA":
                case "$GPGGA":
                    runGGA(name);
                    break;
            }
            saveLogs();
        }
    }

    /**
     * Метод для запуска основных функций для работы с RMC строкой
     *
     * @param name RMC строка
     */
    public void runRMC(String name){
        Log.d(TAG, "RMC: " + name);
        masMName = name.split(",");
        if(masMName != null && masMName.length == 13){
            mName = name;

            LocalDateTime(masMName[9], masMName[1]);
            updateLatLon(masMName[3], masMName[4], masMName[5], masMName[6]);
            updateBearing(masMName[8]);
            updateSpeed(masMName[7]);
        }
    }

    /**
     * Метод для запуска основных функций для работы с GGA строкой
     *
     * @param name GGA строка
     */
    public void runGGA(String name){
        Log.d(TAG, "GGA: " + name);
        mName = name;
        masGGA = name.split(",");
        if(masGGA != null && masGGA.length == 15){
            updateHDOP(masGGA[8]);
        }
    }

    /**
     * Метод обработки текущего времени и даты
     *
     * @param date дата в формате: ddmmyy
     * @param time время в формате: hhmmss
     * @return true, если обработка даты и времени выполнена успешно
     */
    private boolean LocalDateTime(String date, String time){
       //Обработка даты
       if(date.length() != 6){
           return false;
       }

       int month, day, year;
       try{
           day = Integer.parseInt(date.substring(0, 2));
           month = Integer.parseInt(date.substring(2, 4));
           year = 2000 + Integer.parseInt(date.substring(4, 6));
       }
       catch (NumberFormatException nfe){
           Log.d(TAG, "Error parse date!");
           return false;
       }
       mYear = year;
       mMonth = month;
       mDay = day;
      // Log.d(TAG, "Date: " + mDay + "." + mMonth + "." + mYear);

       //Обработка времени
        if(time.length() < 6){
            return false;
        }

        int hour, minute;
        float second;
        try{
            hour = Integer.parseInt(time.substring(0, 2));
            minute = Integer.parseInt(time.substring(2,4));
            second = Float.parseFloat(time.substring(4, time.length()));
        }
        catch (NumberFormatException nfe){
            Log.d(TAG, "Error parsing time" + time);
            return false;
        }
        int isecond = (int) second;
        int millis = (int) ((second - isecond) * 1000);

        Calendar calendar = new GregorianCalendar(TIME_ZONE);
        calendar.set(mYear,mMonth, mDay, hour, minute, isecond);

        long newTime = calendar.getTimeInMillis() + millis;

       // Log.d(TAG, "Time: " + calendar.getTime());
       // Log.d(TAG, "Time2: " + newTime );

        return true;
   }

    /**
     * Метод преобразования координат в формат: HHMM
     *
     * @param coord строка данных координат (широта или долгота)
     * @return преобразованные координаты
     */
    private double convertFromHHMM(String coord){
        double val = Double.parseDouble(coord);
        int degress = (int)(val/100);
        double minutes = val - (degress * 100);
        double dcoord = degress + minutes / 60.0;
        return dcoord;
    }

    /**
     * Метод обработки координат
     *
     * @param latitude широта
     * @param latitudeHemi N северная/ S южная
     * @param longitude долгота
     * @param longitudeHemi E восточная/ W западная
     * @return true, если коордиты успешно преобразованы
     */
    private boolean updateLatLon(String latitude, String latitudeHemi,
                                 String longitude, String longitudeHemi){
        if((latitude.length() == 0) || (longitude.length() == 0)){
            Log.d(TAG, "Получены пустые данные координат! GPS приемник не подключен");
            return false;
        }
        double lat, lon;
        try {
            lat = convertFromHHMM(latitude);
            lon = convertFromHHMM(longitude);

            if(latitudeHemi.equals("S")){
                lat = -lat;
            }
            if(longitudeHemi.equals("W")){
                lon = -lon;
            }
        }
        catch (NumberFormatException e){
            Log.d(TAG, "Error updateLatLon: " + e );
            return false;
        }

        coordinate.setLatsLngs(new GeoPoint(Coordinate.normalizeLatitude(lat), Coordinate.normalizeLongitude(lon)));
        mLatitude = lat;
        mLongitude = lon;

       // Log.d(TAG, " " + mLatitude + " " + mLongitude);

        return true;
    }

    /**
     * Метод обработки направления курса относительно истинного севера (градусы)
     *
     * @param direction направление
     * @return true, если направление успешно обработано
     */
    private boolean updateBearing(String direction){
        if(direction.length() == 0){
            Log.d(TAG, "Error updateHeading");
            return false;
        }
        try {
            bearing = Float.parseFloat(direction);
        }
        catch (NumberFormatException e){
            Log.d(TAG, "Error updateBearing: " + e);
            return false;
        }
       // Log.d(TAG, "--- bearing: " + bearing);

        return true;
    }

    /**
     * Метод обработки HDOP
     *
     * @param hdop HDOP (снижение точности в горизонтальной плоскости)
     * @return true, если HDOP успешно обработан
     */
    private boolean updateHDOP(String hdop){
        if(hdop.length() == 0){
            Log.d(TAG, "Error updateHDOP");
            return false;
        }
        try {
            HDOP = Double.parseDouble(hdop);
        }
        catch (NumberFormatException e) {
            Log.d(TAG, "Error updateHDOP: " + e);
            return false;
        }
        //Log.d(TAG, "HDOP: " + HDOP);

        return true;
    }

    /**
     * Метод обработки горизонтальной скорости
     *
     * @param speedInKnots горизонтальная скорость (узлов в час)
     * @return true, если скорость успешно обработана
     */
    private boolean updateSpeed(String speedInKnots){
        if(speedInKnots.length() == 0){
            Log.d(TAG, "Error updateSpeed");
            return false;
        }
       // Log.d(TAG, "Speed kn/h: " + speedInKnots);
        try {
            //Скорость в км/ч
            speed = Double.parseDouble(speedInKnots) * 1.85;
        }
        catch (NumberFormatException e){
            Log.d(TAG, "Error updateSpeed");
            return false;
        }
        //Log.d(TAG, "Speed km/h: " + speed);

        return true;
    }

    /**
     * Метод для сохранения логов в файл на мобильном устройстве
     * */
    public void saveLogs() {
        writer.print(mName + "\r\n" + "Широта: " + mLatitude + "\r\n" + "Долгота: " + mLongitude + "\r\n" +
                "Направление курса (bearing): " + bearing + " ° \r\n" + "Снижение точности (HDOP): " + HDOP + "\r\n" + "Скорость: " + speed + " км/ч \r\n" +
                "Разница между точками: " + coordinate.pointsDifference() + "\r\n" + "\r\n");
    }
}
