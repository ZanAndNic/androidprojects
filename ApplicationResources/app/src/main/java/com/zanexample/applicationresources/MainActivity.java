package com.zanexample.applicationresources;

import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    private String TAG = "Жизненный цикл";
    private TextView mInfoTextView;
    private Toast toast;
    MediaPlayer mediaPlayer;
    Button startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startButton = (Button) findViewById(R.id.buttonTouchMe);
        mInfoTextView = (TextView) findViewById(R.id.happy_new_year);
        toast  =  Toast.makeText(getApplicationContext(), "onCreate()", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        Log.i(TAG, "onCreate()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        toast  =  Toast.makeText(getApplicationContext(), "onStart()", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        Log.i(TAG, "onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        toast  =  Toast.makeText(getApplicationContext(), "onResume()", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        Log.i(TAG, "onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        toast  =  Toast.makeText(getApplicationContext(), "onPause()", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        Log.i(TAG, "onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        toast  =  Toast.makeText(getApplicationContext(), "onStop()", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

        Log.i(TAG, "onStop()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        toast  =  Toast.makeText(getApplicationContext(), "onRestart()", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        Log.i(TAG, "onRestart()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        toast  =   Toast.makeText(getApplicationContext(), "onDestroy()", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        Log.i(TAG, "onDestroy()");
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonTouchMe:
                startButton.setEnabled(false);
                mInfoTextView.setText(R.string.happy);
                mediaPlayer=MediaPlayer.create(this, R.raw.cola);
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                   @Override
                   public void onCompletion(MediaPlayer mp) {
                       stop();
                   }
               });
                mediaPlayer.start();
                break;
            case R.id.buttonExit:
                finish();
                break;

            default:
                break;
        }
    }

    private void stop(){
        mediaPlayer.stop();
        try {
            mediaPlayer.prepare();
            mediaPlayer.seekTo(0);
        }
        catch (Throwable t) {
            Toast.makeText(this, t.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}

